use deunicode::{deunicode, deunicode_char};
use rand::seq::SliceRandom;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs;

const MIN_SCORE: i32 = -4;

impl<'b> Game {
    pub fn new<'c>() -> Game {
        let args: Vec<String> = std::env::args().collect();
        let file = match args.get(1) {
            Some(val) => val.to_string(),
            None => panic!("No file specified."),
        };

        let (title, total_score, highscores, questions) = read_file(&file);
        let total_questions = questions.len() as i32;

        Game {
            questions,
            total_score,
            file,
            total_questions,
            highscores,
            title,
        }
    }

    pub fn num_questions(&self) -> i32 {
        self.questions.iter().filter(|(_, v)| (v.score < 0)).count() as i32
    }

    pub fn reset_scores(&mut self) {
        for (_,q) in &mut self.questions {
            q.score = -1;
        }
        self.total_score = 0;
    }


    pub fn new_question<'a>(&self, prev_question: Option<String>) -> Result<String, String> {

        let mut candidates = self
            .questions
            .iter()
            .map(|(_, v)| v)
            .filter(|v| (v.score < 0))
            .collect::<Vec<&Question>>();

        
        if candidates.len() != 1 {
            if let Some(q) = prev_question {
                candidates = candidates
                    .iter()
                    .filter(|v| (v.key != q))
                    .map(|&i| i)
                    .collect::<Vec<&Question>>();
            }
        }

        match candidates.choose_mut(&mut rand::thread_rng()) {
            Some(question) => {
                let q = question.key.clone();
                return Ok(q);
            }
            None => Err(format!("No questions left")),
        }
    }

    pub fn guess(&mut self, guess: &str, key: &str) -> Result<String, String> {
        let mut question = self.questions.get_mut(key).unwrap();

        let parsed_guess: String = match guess.trim_end_matches("\n").parse() {
            Ok(parsed_guess) => parsed_guess,
            Err(_) => return Err(format!("Your guess could not be parsed")),
        };

        if correct(&parsed_guess, &question.answer) {
            question.score += question.potential_score;
            question.potential_score = 1;

            if question.score >= 0 {
                self.total_score += 1;
                return Ok(format!(
                    "Correct! <span weight='bold'>{}</span> is off the list. Next question:",
                    question.answer
                ));
            } else {
                return Ok(format!(
                    "<span weight='bold'>{}</span> is correct! Next question:",
                    question.answer
                ));
            }
        }
        if question.potential_score > MIN_SCORE {
            question.potential_score -= 1;
            self.total_score -=1;
        } else {
            return Err(format!(
                "The correct answer is <span weight='bold'>{}</span>.",
                &question.answer
            ));
        }

        let hint = hint(&parsed_guess, &question.answer);

        if correct(&hint, &question.answer) {
            return Err(format!(
                "The correct answer is <span weight='bold'>{}</span>.",
                &question.answer
            ));
        } else {
            return Err(format!("Hint: {}…", hint));
        }
    }

    pub fn quit(&self) {
        write_file(&self.questions, &self.title, self.total_score, &self.highscores, &self.file);
    }
}

pub struct Game {
    questions: HashMap<String, Question>,
    pub total_score: i32,
    pub total_questions: i32,
    file: String,
    pub highscores: Vec<i32>,
    pub title: String,
}

struct Question {
    key: String,
    answer: String,
    score: i32,
    potential_score: i32,
}

fn correct(guess: &str, correct: &str) -> bool {
    match deunicode(&guess.to_uppercase()).cmp(&deunicode(&correct).to_uppercase()) {
        Ordering::Equal => true,
        _ => false,
    }
}

fn hint(guess: &str, correct: &str) -> String {
    let guess = format!("{:width$}", guess.to_uppercase(), width = correct.len());
    let mut hint = String::new();
    // break the loop when the characters no longer match.
    'outer: for (answer, guessed) in correct.chars().zip(guess.chars()) {
        // use the original character for the hint
        hint.push(answer);
        // but use uppercase versions for comparison
        for (c1, c2) in answer.to_uppercase().zip(guessed.to_uppercase()) {
            // a single character may map to multiple in uppercase, hence the inner loop
            match deunicode_char(c1).cmp(&deunicode_char(c2)) {
                Ordering::Less | Ordering::Greater => break 'outer,
                Ordering::Equal => continue,
            }
        }
    }
    hint
}

fn read_file(filename: &str) -> (String, i32, Vec<i32>, HashMap<String, Question>) {
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let mut questions = HashMap::new();

    let everything: Vec<&str> = contents.splitn(2, "\n---\n").collect();

    let (header, lines) = match everything.as_slice() {
        [head, tail] => (*head, *tail),
        _ => panic!("Could not separate header section"),
    };

    let intro_fields: Vec<&str> = header.splitn(3,"\n").collect();

    let (subject, score, highscores) = match intro_fields.as_slice() {
        [head, mid, tail] => (*head, *mid, *tail),
        _ => panic!(),
    };

    let score: Vec<&str> = score.split("\t").collect();
    let score: i32 = match score[1].parse() {
        Ok(v) => v,
        Err(_) => panic!("Score could not be parsed as an integer")
    };

    let mut a_highscores: Vec<i32> = vec!();

    for line in highscores.split("\n") {
        let highscore: i32 = match line.split("\t").collect::<Vec<&str>>()[1].parse() {
            Ok(v) => v,
            Err(_) => panic!("Score could not be parsed as an integer")
        };
        a_highscores.push(highscore);
    }

    for line in lines.split("\n") {
        let question: Vec<&str> = line.split("\t").collect();

        if question.len() == 1 {
            // empty line returns 1 element
            continue;
        } else if question.len() == 3 {

            let score = match String::from(question[2]).parse() {
                Ok(v) => v,
                Err(_) => panic!("Score could not be parsed as an integer: '{}'",line)
            };

            let c = Question {
                key: String::from(question[0]),
                answer: String::from(question[1]),
                score: score,
                potential_score: 1,
            };
            let key = c.key.clone();
            questions.insert(key, c);
        } else {
            panic!("Question unparseable: {} {}", line, question.len())
        }
    }
    (String::from(subject), score, a_highscores, questions)
}

fn write_file(questions: &HashMap<String, Question>, title: &str, score: i32, highscores: &Vec<i32>, filename: &str) {
    let mut output = String::new();

    output.push_str(&format!("{}\n", title));
    output.push_str(&format!("Current score\t{}\n", score.to_string()));
    for highscore in highscores {
        output.push_str(&format!("Highscore\t{}\n", highscore.to_string()));
    }
    output.push_str("---\n");

    for (_, question) in questions {
        let line = format!(
            "{}\t{}\t{}\n",
            &question.key,
            &question.answer,
            &question.score.to_string()
        );
        output.push_str(&line);
    }
    fs::write(filename, output).expect("Something went wrong writing to the file");
}
