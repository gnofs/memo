use game::Game;
use gtk::prelude::*;
use std::sync::{Arc, Mutex};

mod game;

fn main() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }

    let window = gtk::Window::new(gtk::WindowType::Toplevel);
    let frame = gtk::Frame::new(None);
    let hbox = gtk::Box::new(gtk::Orientation::Vertical, 10);
    let vbox = gtk::Box::new(gtk::Orientation::Horizontal, 10);
    let info_label = gtk::Label::new(None);
    let question_label = gtk::Label::new(None);
    let storage_label = gtk::Label::new(None);
    let score_label = gtk::Label::new(None);
    let entry = gtk::Entry::new();
    let image = gtk::Image::new();
    let button = gtk::Button::new_with_label("Reset");

    entry.set_placeholder_text(Some("Enter your answer…"));
    frame.set_border_width(30);
    hbox.set_border_width(30);

    info_label.set_justify(gtk::Justification::Left);
    question_label.set_justify(gtk::Justification::Left);
    window.set_title("Memo");
    window.set_position(gtk::WindowPosition::Center);
    //window.set_default_size(1200, 700);
    window.add(&frame);

    let current_game = Arc::new(Mutex::new(Game::new()));

    let game_clone = current_game .clone();
    let entry_clone = entry.clone();
    let info_clone = info_label.clone();
    let image_clone = image.clone();
    let question_clone = question_label.clone();
    let score_clone = score_label.clone();
    let storage_clone = storage_label.clone();
    let new_question = move |prev_question: String| {
        let g = game_clone.lock().unwrap();
        match g.new_question(Some(prev_question)) {
            Ok(question) => {
                match question.get(..7) {
                    None => {
                        question_clone
                            .set_markup(&format!("<span weight='bold'>{}</span>", question));
                    }
                    Some(slice) => {
                        if slice == "file://" {
                            let filename = &question[7..];
                            image_clone.set_from_file(filename);
                        } else {
                            question_clone
                                .set_markup(&format!("<span weight='bold'>{}</span>", question));
                        }
                    }
                }

                score_clone.set_text(&format!(
                    "{} points, with {}/{} questions left.",
                    g.total_score,
                    g.num_questions(),
                    g.total_questions
                ));
                storage_clone.set_text(&question);
            },
            Err(msg) => {
                info_clone.set_text(&format!("All done!"));
                score_clone.set_text(&format!("Total score: {}, out a possible {}.", g.total_score, g.total_questions));
                image_clone.clear();
                question_clone.set_text("");
                entry_clone.set_sensitive(false);
                entry_clone.set_editable(false);
                entry_clone.set_placeholder_text(Some(&msg));
                entry_clone.grab_focus();
            }
        }
    };

    let image_clone = image.clone();
    let entry_clone = entry.clone();
    let info_clone = info_label.clone();
    let question_clone = question_label.clone();
    let score_clone = score_label.clone();
    let storage_clone = storage_label.clone();
    let game_clone = current_game.clone();
    let frame_clone = frame.clone();
    let new_game = move || -> Option<gtk::ResponseType> {
        let game = game_clone.lock().unwrap();
        frame_clone.set_label(Some(&game.title));
        match game.new_question(None) {
            Ok(question) => {
                match question.get(..7) {
                    None => {
                        question_clone
                            .set_markup(&format!("<span weight='bold'>{}</span>", question));
                    }
                    Some(slice) => {
                        if slice == "file://" {
                            let filename = &question[7..];
                            image_clone.set_from_file(filename);
                        } else {
                            question_clone
                                .set_markup(&format!("<span weight='bold'>{}</span>", question));
                        }
                    }
                }
                score_clone.set_text(&format!(
                    "{} points, with {}/{} questions left.",
                    game.total_score,
                    game.num_questions(),
                    game.total_questions
                ));
                storage_clone.set_text(&question);
            None
            },
            Err(msg) => {
                info_clone.set_text(&format!("All done!"));
                score_clone.set_text(&format!("Total score: {}, out a possible {}.", game.total_score, game.total_questions));
                image_clone.clear();
                question_clone.set_text("");
                entry_clone.set_sensitive(false);
                entry_clone.set_editable(false);
                entry_clone.set_placeholder_text(Some(&msg));
                entry_clone.grab_focus();
                None
            }
        }
    };


    let game_clone = current_game.clone();
    let entry_clone = entry.clone();
    let new_game_clone = new_game.clone();
    let window_clone = window.clone();
    button.connect_clicked(move |_| {

        let dialog = gtk::MessageDialog::new(Some(&window_clone),
            gtk::DialogFlags::MODAL|gtk::DialogFlags::DESTROY_WITH_PARENT,
            gtk::MessageType::Question,
            gtk::ButtonsType::YesNo,
            "Reset scores and restart?"
            );

        if let gtk::ResponseType::Yes = dialog.run() {
            game_clone.lock().unwrap().reset_scores();
            new_game_clone();
            entry_clone.set_sensitive(true);
            entry_clone.set_editable(true);
            entry_clone.grab_focus();
        }
        dialog.destroy();
    });

    let entry_clone = entry.clone();
    let info_clone = info_label.clone();
    let game_clone = current_game.clone();
    let storage_clone = storage_label.clone();
    let new_question_clone = new_question.clone();
    window.connect_key_press_event(move |_, key| {
        let keyval = key.get_keyval();

        if keyval == 65293 {
            let text = &String::from(entry_clone.get_text().unwrap());
            entry_clone.set_text("");
            let mut this_game = game_clone.lock().unwrap();
            let question = String::from(storage_clone.get_text().unwrap());
            let hint = this_game.guess(text, &question);
            match hint {
                Ok(hint) => {
                    info_clone.set_markup(&format!("<span weight='light'>{}</span>", &hint));
                    drop(this_game);
                    new_question_clone(question);
                }
                Err(msg) => {
                    info_clone.set_markup(&msg);
                }
            }
        }
        Inhibit(false)
    });


    let game_clone = current_game.clone();
    window.connect_delete_event(move |_, _| {
        game_clone.lock().unwrap().quit();
        gtk::main_quit();
        Inhibit(false)
    });

    vbox.pack_start(&question_label,true,true,0);
    vbox.pack_start(&button,true,false,0);
    hbox.pack_start(&info_label,true,false,0);
    hbox.pack_start(&vbox,true,false,0);
    hbox.pack_start(&image,false,false,0);
    hbox.pack_start(&entry,false,false,0);
    hbox.pack_start(&score_label,false,false,0);
    frame.add(&hbox);
    window.set_decorated(true);
    window.show_all();

    new_game.clone()();
    gtk::main();
}
